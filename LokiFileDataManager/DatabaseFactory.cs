using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace LokiFileDataManager {
	public class DatabaseFactory : IDesignTimeDbContextFactory<DatabaseContext>
	{
		public DatabaseContext CreateDbContext(string[] args)
		{
			return new DatabaseContext("Data Source=/home/lokilokus/lokifile.db");
		}
	}
}