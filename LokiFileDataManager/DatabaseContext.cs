using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace LokiFileDataManager {
	public class DatabaseContext : DbContext {
		public DbSet<LokiInternalDirectory> Directories { get; set; }
		public DbSet<LokiInternalFile> Files { get; set; }
		private readonly string _connectionStr;

		public DatabaseContext(string conStr)
		{
			_connectionStr = conStr;
		}
		
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite(_connectionStr);
		}
	}

	public class LokiInternalFile {
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public string Id { get; set; }

		public string FileName { get; set; }

		[ForeignKey("Directory")] public string DirectoryId { get; set; }

		public string CustomData { get; set; }


		public LokiInternalDirectory Directory { get; set; }
	}

	public class LokiInternalDirectory {
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public string Id { get; set; }

		public string DirectoryName { get; set; }
		public string CustomData { get; set; }

		[ForeignKey("Directory")] public string DirectoryId { get; set; }

		public LokiInternalDirectory Directory { get; set; }
	}
}