using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using LokiFile;
using LokiFile.DataManager;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using DirectoryNotFoundException = LokiFile.DirectoryNotFoundException;

namespace LokiFileDataManager {
	public class SqliteDataManager : IDataManager {
		
		private string _dbConnectionStr;

		public SqliteDataManager(string connectionString)
		{
			DatabaseContext _dbContext;
			_dbConnectionStr = connectionString;
			_dbContext = new DatabaseContext(connectionString);
			try
			{
				_dbContext.Database.EnsureCreated();
				_dbContext.Database.Migrate();
			}
			catch (Exception e)
			{
			}
			
			IQueryable<LokiInternalDirectory> rootDirs = _dbContext.Directories.Where(x => x.DirectoryId == null);
			if (rootDirs.Count() > 1) throw new TooMuchRootDirectoriesException();
			if (!rootDirs.Any())
				_dbContext.Directories.Add(new LokiInternalDirectory
				{
					DirectoryName = "",
					CustomData = "Root Dir",
					DirectoryId = null
				});
			var t =_dbContext.SaveChanges();
		
		}

		public bool ExistsFile(string filePath)
		{
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			
			if (filePath == null) throw new ArgumentNullException();
			filePath = LokiPath.NormalizePath(filePath);
			string[] dirs = LokiPath.SplitPath(filePath);
			try
			{
				LokiInternalDirectory rootDir = dbCtxt.Directories.SingleOrDefault(x => x.DirectoryId == null);
				foreach (string dir in dirs)
				{
					if (dirs.Last() == dir)
						return dbCtxt.Files.Any(x => x.FileName == dir && x.DirectoryId == rootDir.Id);
					rootDir = dbCtxt.Directories.SingleOrDefault(x =>
						x.DirectoryName == dir && x.DirectoryId == rootDir.Id);
					if (rootDir == null) return false;
				}
			}
			catch (Exception e)
			{
			}
			

			return false;
		}

		public bool ExistsDirectory(string dirPath)
		{
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			
			
			if (dirPath == null) throw new ArgumentNullException();
			dirPath = LokiPath.NormalizePath(dirPath);
			string[] dirs = LokiPath.SplitPath(dirPath);
			LokiInternalDirectory rootDir = dbCtxt.Directories.SingleOrDefault(x => x.DirectoryId == null);
			for (int i = 1; i < dirs.Length; i++)
			{
				if (dirs[i] == "") return rootDir != null; //TODO if last idx
				string tmp = rootDir.Id;
				rootDir = dbCtxt.Directories.SingleOrDefault(x =>
					x.DirectoryName == dirs[i] && x.DirectoryId == tmp);
				if (rootDir == null) return false;
			}

			return true;
		}

		public string GetFileIdFromPath(string filePath)
		{
			if (filePath == null) throw new ArgumentNullException();
			filePath = LokiPath.NormalizePath(filePath);
			string[] dirs = LokiPath.SplitPath(filePath);
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);

			LokiInternalDirectory rootDir = dbCtxt.Directories.SingleOrDefault(x => x.DirectoryId == null);
			foreach (string dir in dirs)
			{
				if (dirs.Last() == dir)
				{
					LokiInternalFile result =
						dbCtxt.Files.SingleOrDefault(x => x.FileName == dir && x.DirectoryId == rootDir.Id);
					if (result == null) throw new FileNotFoundException();
					return result.Id;
				}

				rootDir = dbCtxt.Directories.SingleOrDefault(x =>
					x.DirectoryName == dir && x.DirectoryId == rootDir.Id);
				if (rootDir == null) throw new FileNotFoundException();
			}

			return rootDir.Id;
		}

		public string GetDirectoryIdFromPath(string dirPath)
		{
			if (dirPath == null) throw new ArgumentNullException();
			if(!LokiPath.DirectoryPathValid(dirPath)) throw new NameNotValidException();
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			dirPath = LokiPath.NormalizePath(dirPath);
			var dirs = LokiPath.SplitPath(dirPath);
			
			LokiInternalDirectory rootDir = dbCtxt.Directories.SingleOrDefault(x => x.DirectoryId == null);
			
			for (int i = 1; i < dirs.Length; i++)
			{
				if (i == dirs.Length-1) return rootDir.Id;
				
				rootDir = dbCtxt.Directories.SingleOrDefault(x =>
					x.DirectoryName == dirs[i] && x.DirectoryId == rootDir.Id);
				if(rootDir == null) throw new DirectoryNotFoundException();
			}
			throw new DirectoryNotFoundException();
		}

		public LokiFileInfo GetFile(string fileId)
		{
			if (fileId == null) throw new ArgumentNullException();
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			LokiInternalFile result = dbCtxt.Files.SingleOrDefault(x => x.Id == fileId);
			if (result == null) throw new FileNotFoundException();
			return new LokiFileInfo
			{
				Id = result.Id,
				Path = GetPathFromFile(result),
				CustomData = result.CustomData,
				Name = result.FileName
			};
		}

		public LokiDirectoryInfo GetDirectory(string dirId)
		{
			if (dirId == null) throw new ArgumentNullException();
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			LokiInternalDirectory result = dbCtxt.Directories.SingleOrDefault(x => x.Id == dirId);
			if (result == null) throw new FileNotFoundException();
			return new LokiDirectoryInfo
			{
				Id = result.Id,
				Path = GetPathFromDirectory(result),
				CustomData = result.CustomData,
				
				DirectoryName = result.DirectoryName
			};
		}

		public LokiFileInfo CreateFile(string filename, string customData, string dirId)
		{
			if (!ExistsDirectoryById(dirId)) throw new System.IO.DirectoryNotFoundException();
			//TODO File exists
			if (!LokiPath.IsFileNameValid(filename)) throw new NameNotValidException();
			LokiInternalFile tmpFile = new LokiInternalFile
			{
				DirectoryId = dirId,
				FileName = filename,
				CustomData = customData
			};
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			dbCtxt.Files.Add(tmpFile);
			return new LokiFileInfo
			{
				Id = tmpFile.Id,
				CustomData = tmpFile.CustomData,
				Name = tmpFile.FileName,
				Path = GetPathFromFile(tmpFile)
			};
		}

		private bool ExistsDirectoryById(string dirId)
		{
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			return dbCtxt.Directories.Any(x => x.Id == dirId);
		}

		public LokiDirectoryInfo CreateDirectory(string dirname, string customData, string dirId)
		{
			if (!LokiPath.IsDirectoryNameValid(dirname)) throw new NameNotValidException();
			if(!ExistsDirectoryById(dirId)) throw new DirectoryNotFoundException();
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			LokiInternalDirectory tmpFile = new LokiInternalDirectory
			{
				DirectoryId = dirId,
				DirectoryName = dirname,
				CustomData = customData
			};
			dbCtxt.Directories.Add(tmpFile).State = EntityState.Added;
			dbCtxt.SaveChanges();
			return new LokiDirectoryInfo
			{
				Id = tmpFile.Id,
				CustomData = tmpFile.CustomData,
				DirectoryName = tmpFile.DirectoryName,
				Path = GetPathFromDirectory(tmpFile)
			};
		}

		public void UpdateFileCustomData(string fileId, string data)
		{
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			LokiInternalFile file = dbCtxt.Files.SingleOrDefault(x => x.Id == fileId);
			if (file == null) throw new FileNotFoundException();
			file.CustomData = data;
			dbCtxt.SaveChanges();
		}

		public void UpdateDirectoryCustomData(string dirId, string data)
		{
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			LokiInternalDirectory directory = dbCtxt.Directories.SingleOrDefault(x => x.Id == dirId);
			if (directory == null) throw new DirectoryNotFoundException();
			directory.CustomData = data;
			dbCtxt.SaveChanges();
		}

		public void DeleteFile(string fileId)
		{
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			LokiInternalFile file = dbCtxt.Files.SingleOrDefault(x => x.Id == fileId);
			if (file == null) throw new FileNotFoundException();
			dbCtxt.Files.Remove(file);
			dbCtxt.SaveChanges();
		}

		public void DeleteDirectory(string dirId, bool deleteRecursive)
		{
			if(dirId == null) throw new ArgumentNullException();
			if(!ExistsDirectoryById(dirId)) throw new DirectoryNotFoundException();
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			IEnumerable<LokiFileInfo> tmpFiles = GetFiles(dirId);
			IEnumerable<LokiDirectoryInfo> tmpDirs = GetDirectories(dirId);
			if(!deleteRecursive && (tmpFiles.Any() || tmpDirs.Any())) throw new DirectoryNotEmptyException();
			foreach (LokiFileInfo lokiFileInfo in tmpFiles)
			{
				LokiFile.LokiFile.Delete(lokiFileInfo.Path);
			}

			foreach (LokiDirectoryInfo lokiDirectoryInfo in tmpDirs)
			{
				DeleteDirectory(lokiDirectoryInfo.Path, true);
			}

			var dir = dbCtxt.Directories.SingleOrDefault(x => x.Id == dirId);
			dbCtxt.Directories.Remove(dir);
			dbCtxt.SaveChanges();


		}

		public IEnumerable<LokiFileInfo> GetFiles(string dirId, bool recursive)
		{
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			LokiInternalDirectory directory = dbCtxt.Directories.SingleOrDefault(x => x.Id == dirId);
			if (directory == null) throw new DirectoryNotFoundException();
			IEnumerable<LokiFileInfo> result = dbCtxt.Files.Where(x => x.DirectoryId == dirId).Select(x =>
				new LokiFileInfo
				{
					Id = x.Id,
					CustomData = x.CustomData,
					Name = x.FileName,
					Path = GetPathFromFile(x)
				}).AsEnumerable();
			if (!recursive) return result;
			//TODO Continue
			throw new NotImplementedException();
		}

		public IEnumerable<LokiDirectoryInfo> GetDirectories(string dirId, bool recursive)
		{
			if(dirId == null) throw new ArgumentNullException();
			if(!ExistsDirectoryById(dirId)) throw new DirectoryNotFoundException();
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			List<LokiDirectoryInfo> result;
			//LokiInternalDirectory dir = _dbContext.Directories.SingleOrDefault(x => x.Id == dirId);
			result = dbCtxt.Directories.Where(x => x.DirectoryId == dirId).Select(x => new LokiDirectoryInfo()
			{
				Path = GetPathFromDirectory(x),
				CustomData = x.CustomData,
				DirectoryName = x.DirectoryName,
				Id = x.Id
			}).ToList();
			if (!recursive) return result;
			List<LokiDirectoryInfo> tmpList = new List<LokiDirectoryInfo>();
			foreach (LokiDirectoryInfo directoryInfo in result)
			{
				IEnumerable<LokiDirectoryInfo> tmp =  GetDirectories(directoryInfo.Id,true);
				tmpList.AddRange(tmp);
			}
			result.AddRange(tmpList);
			return result;

		}

		public bool DirectoryIsEmpty(string dirId)
		{
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			if (!dbCtxt.Directories.Any(x => x.DirectoryId == dirId)) throw new DirectoryNotFoundException();
			return dbCtxt.Files.Any(x => x.DirectoryId == dirId) ||
			       dbCtxt.Directories.Any(x => x.DirectoryId == dirId);
		}

		private string GetPathFromFile(LokiInternalFile file)
		{
			LokiInternalDirectory directory = new LokiInternalDirectory {DirectoryId = file.DirectoryId};
			string result = "";
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			do
			{
				directory = dbCtxt.Directories.Single(x => x.Id == directory.DirectoryId);
				result = directory + LokiPath.SEPERATOR + result;
			} while (directory.DirectoryId != null);

			return result;
		}

		private string GetPathFromDirectory(LokiInternalDirectory dir)
		{
			LokiInternalDirectory directory = dir;
			string result = dir.DirectoryName;
			DatabaseContext dbCtxt = new DatabaseContext(_dbConnectionStr);
			do
			{
				directory = dbCtxt.Directories.SingleOrDefault(x => x.Id == directory.DirectoryId);
				if (directory != null)
					result = directory.DirectoryName + LokiPath.SEPERATOR + result;
				else
					result = LokiPath.SEPERATOR + result;
			} while (directory?.DirectoryId != null);

			return result;
		}
	}

	public class TooMuchRootDirectoriesException : Exception {
	}
}