using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LokiFile;
using LokiFileBlobManager;
using LokiFileDataManager;
using Microsoft.EntityFrameworkCore.Internal;
using Xunit;

namespace LokiFileTester {
	public class DirectoryCreate : IDisposable{
		public DirectoryCreate()
		{
			string conStr = "Data Source=" + Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
				"lokifile.db");
			
			
			DirectoryInfo directoryInfo = new DirectoryInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
				"FileBlobStorage/"));
			if(!directoryInfo.Exists) directoryInfo.Create();
			
			LokiFile.LokiFile.Init(new FileBlobStorage(directoryInfo.FullName),
				new SqliteDataManager(conStr));
		}
		
		[Fact]
		public void CreateDirectoryWithNull()
		{
			Assert.Throws<ArgumentNullException>(() => LokiDirectory.CreateDirectory(null));
		}

		[Theory]
		[InlineData("/Haddllo/das","Jo")]
		[InlineData("/das/das",null)]
		[InlineData("/asdjsa/","Hallo asd74zl,32äääääölasdlashdu")]
		[InlineData("/dassfd/",null)]
		[InlineData("/das/","Jo")]
		[InlineData("/asdhgfa",null)]
		[InlineData("/asdasd/",null)]
		[InlineData("/",null)]
		[InlineData("/das/da/")]
		[InlineData("/asdfasd/")]
		[InlineData("/asdfasd/asd")]
		[InlineData("/asdfasd/asd/asdfg")]
		[InlineData("/asdfasd/asd/asdfg/ksdfksd")]
		public void CreateDirectory(string dir, string customData = null)
		{
			try
			{
				LokiDirectoryInfo tmp = LokiDirectory.CreateDirectory(dir, customData);
				Assert.NotNull(tmp);
			}
			catch (DirectoryExistsException e)
			{
			}
			catch (Exception e)
			{
				throw e;
			}
			finally
			{
				Assert.NotNull(LokiDirectory.GetDirectory(dir));
			}
		}

		[Theory]
		[InlineData("/")]
		[InlineData("")]
		[InlineData(null)]
		[InlineData("/äülp")]
		[InlineData("/ /")]
		[InlineData("/9234//")]
		[InlineData("/\\")]
		[InlineData("/&%$")]
		public void NotValidDirectoryCreate(string path, string data = null)
		{
			try
			{
				LokiDirectory.CreateDirectory(path, data);

				Assert.True(false);
			}
			catch (Exception e)
			{
				Assert.True(true);
			}
		}

		[Theory]
		[InlineData(("/"))]
		public void GetRootDirectory(string dir)
		{
			IEnumerable<LokiDirectoryInfo> data = LokiDirectory.GetDirectories(dir,true);
			
			Assert.True(data.Any());
		}

		[Theory]
		[InlineData("/")]
		public void DeleteDirectory(string dirName)
		{
			LokiDirectory.Delete(dirName,true);
			Assert.False(LokiDirectory.GetDirectories(dirName).Any());
		}

		public void Dispose()
		{
			LokiDirectory.Delete("/",true);
		}
	}
}