# LokiFile
# Class Structure

## Backend
### IBlobStorage : Interface
Manage FileAccess
 bool WriteAsync(string id, string data);
 bool WriteAsync(string id, string data, FileAccessMode fmode);
 bool WriteAsync(string id, byte[] data); //true on Overwrite
 bool WriteAsync(string id, byte[] data, FileAccessMode fmode);//true on Overwrite
 bool ExistsAsync(string id);
 int GetSizeAsync(string id);
 bool Delete(string id); //true if file was there
 byte[] ReadAllBytesAsync(string id);
 string ReadAllTextAsync(string id);
 string[] ReadAllLinesAsync(string);
 byte[] GetSHA256HashAsync(string id);
 
### FileWriteMode : Enum
Overwrite,
Append,


### IDataManager
    bool ExistsFile(string filePath);
    bool ExistsDirectory(string dirPath);

    string GetFileIdFromPath(string filePath);
    string GetDirectoryIdFromPath(string dirPath);

    LokiFileInfo GetFile(string id);

    LokiDirectoryInfo GetDirectory(string id);

    LokiFileInfo CreateFile(string filename,string customData, string dirId);
    LokiFileInfo CreateDirectory(string filename,string customData, string dirId);

    void UpdateCustomData(string id);

    void DeleteFile(string fileId);
    void DeleteDirectory(string dirIf,bool deleteRecursive);

    IEnumerable<LokiFileInfo> GetFiles(string dirId,bool recursive);
    IEnumerable<LokiDirectoryInfo> GetDirectories(string dirId,bool recursive);
    bool DirectoryIsEmpty(string dirId);






Path to ID Bidirectional
Existsfile
ExistsDir
Createfile
CreateDir
DeleteDir
DeleteFile
GetDirOfFile
GetDirDir 






## Frontend
### LokiFile
LokiFileInfo Create(string path);
LokiFileInfo Create(string path, string customdata);

void UpdateCustomDataById(string id);
void UpdateCustomData(string path);

bool WriteAsync(string id, string data);
bool WriteAsync(string id, string data, FileAccessMode fmode);
bool WriteAsync(string id, byte[] data); //true on Overwrite
bool WriteAsync(string id, byte[] data, FileAccessMode fmode);
bool Delete(string path);
bool DeleteById(string id);
byte[] ReadAllBytesAsync(string id);
string ReadAllTextAsync(string id);
string[] ReadAllLinesAsync(string);
int GetSizeAsync(string id);
void Move(string sourcePath,string destinationPath);
void MoveById(string sourceId,string destinationPath);
bool ExistsAsync(string path);
byte[] GetSHA256HashAsync(string path);
byte[] GetSHA256HashByIdAsync(string id);
LokiDirectoryInfo GetDirectoryById(string id);
LokiDirectoryInfo GetDirectory(string path);



### LokiDirectory
Get Parent
Get Children


LokiDirectoryInfo Create(string path);
bool Delete(string path); //Delete(path,false) => if Dir not Empty => exception false if Dir not exists
bool Delete(string path, bool deleteRecursive)
void Move(string sourcePath, string destinationPath)
IEnumerable<string> EnumerableFiles(string path);
bool Exists(string path);
LokiDirectoryInfo GetDirectory(string path);
LokiDirectoryInfo GetDirectory(string path, bool createIfNotExists);
LokiDirectoryInfo GetParent(string path);
LokiDirectoryInfo GetParentById(string id);
IEnumerable<LokiDirectoryInfo> GetDirectories(string path)
IEnumerable<LokiDirectoryInfo> GetDirectoriesById(string id)
IEnumerable<LokiFileInfo> GetFiles(string path)
IEnumerable<LokiFileInfo> GetFilesById(string id)


LokiDirectoryInfo Create(string path);
LokiDirectoryInfo Create(string path, string customData);

void UpdateCustomDataById(string id);
void UpdateCustomData(string path);

