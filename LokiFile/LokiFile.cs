using System;
using System.IO;
using System.Threading.Tasks;
using LokiFile.BlobStorage;
using LokiFile.DataManager;

namespace LokiFile {
	public static class LokiFile {
		private static IBlobStorage _blob;

		private static IDataManager _data;

		internal static IBlobStorage _blobStorage
		{
			get
			{
				if (_blob == null) throw new NotInitilizedException();
				return _blob;
			}
			private set => _blob = value;
		}

		internal static IDataManager _dataManager
		{
			get
			{
				if (_data == null) throw new NotInitilizedException();
				return _data;
			}
			private set => _data = value;
		}

		public static void Init(IBlobStorage blobStorage, IDataManager dataManager)
		{
			_blobStorage = blobStorage;
			_dataManager = dataManager;
		}


		/// <summary>
		///     Creates a new File without Data
		/// </summary>
		/// <param name="path"></param>
		/// <param name="customData"></param>
		/// <param name="overwrite"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		/// <exception cref="NameNotValidException"></exception>
		/// <exception cref="FileExistsException"></exception>
		public static LokiFileInfo Create(string path, string customData = null, bool overwrite = false)
		{
			if (path == null) throw new ArgumentNullException();
			bool fileExists = _dataManager.ExistsFile(path);
			if (!overwrite && fileExists) throw new FileExistsException();
			if (fileExists)
				try
				{
					string fileId = _dataManager.GetFileIdFromPath(path);
					_dataManager.DeleteFile(fileId);
					_blobStorage.DeleteFile(fileId);
				}
				catch (Exception e)
				{
					//On File not Found => No Problems
				}

			string dirId = _dataManager.GetDirectoryIdFromPath(path);
			LokiFileInfo result = _dataManager.CreateFile(LokiPath.GetFileNameFromPath(path), customData, dirId);
			_blobStorage.CreateFile(result.Id);
			return result;
		}

		/// <summary>
		///     Updates the Custom data from the given File
		/// </summary>
		/// <param name="fileId"></param>
		/// <param name="data"></param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static void UpdateCustomDataById(string fileId, string data)
		{
			if (fileId == null || data == null) throw new ArgumentNullException();
			_dataManager.UpdateFileCustomData(fileId, data);
		}

		/// <summary>
		///     Updates the Custom data from the given File
		/// </summary>
		/// <param name="path"></param>
		/// <param name="data"></param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static void UpdateCustomData(string path, string data)
		{
			if (path == null || data == null) throw new ArgumentNullException();
			string fileId = _dataManager.GetFileIdFromPath(path);
			UpdateCustomDataById(fileId, data);
		}

		/// <summary>
		///     Checks if File Exists
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		public static bool Exists(string path)
		{
			if (path == null) throw new ArgumentNullException();
			return _dataManager.ExistsFile(path);
		}

		/// <summary>
		///     Checks if File has Data written
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		public static bool HasContent(string path)
		{
			if (path == null) throw new ArgumentNullException();
			return HasContentByFileId(_dataManager.GetFileIdFromPath(path));
		}

		/// <summary>
		///     Checks if File has Data written
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static bool HasContentByFileId(string fileId)
		{
			if (fileId == null) throw new ArgumentNullException();
			return _blobStorage.GetSize(fileId) == 0;
		}

		/// <summary>
		///     Writes Data to File
		/// </summary>
		/// <param name="fileId"></param>
		/// <param name="data"></param>
		/// <param name="fMode"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static async Task WriteAsync(string fileId, string data, FileAccessMode fMode = FileAccessMode.Overwrite)
		{
			if (fileId == null) throw new ArgumentNullException();
			await _blobStorage.WriteAsync(fileId, data, fMode);
		}

		/// <summary>
		///     Writes Data to File
		/// </summary>
		/// <param name="fileId"></param>
		/// <param name="data"></param>
		/// <param name="fMode"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static async Task WriteAsync(string fileId, byte[] data, FileAccessMode fMode = FileAccessMode.Overwrite)
		{
			if (fileId == null) throw new ArgumentNullException();
			await _blobStorage.WriteAsync(fileId, data, fMode);
		}

		/// <summary>
		///     Deletes the file with given FileId
		/// </summary>
		/// <param name="fileId"></param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static void Delete(string path)
		{
			if (path == null) throw new ArgumentNullException();
			string fileId = _dataManager.GetFileIdFromPath(path);
			DeleteById(fileId);
		}

		/// <summary>
		///     Deletes the file with given FileId
		/// </summary>
		/// <param name="fileId"></param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static void DeleteById(string fileId)
		{
			if (fileId == null) throw new ArgumentNullException();
			_dataManager.DeleteFile(fileId);
			_blobStorage.DeleteFile(fileId);
		}

		/// <summary>
		///     Read All Bytes from the File
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static Task<byte[]> ReadAllBytes(string filePath)
		{
			if (filePath == null) throw new ArgumentNullException();
			string fileId = _dataManager.GetFileIdFromPath(filePath);
			return ReadAllBytesById(fileId);
		}

		/// <summary>
		///     Read All Bytes from the File
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static Task<byte[]> ReadAllBytesById(string fileId)
		{
			if (fileId == null) throw new ArgumentNullException();
			return _blobStorage.ReadAllBytesAsync(fileId);
		}

		/// <summary>
		///     Read All Bytes from the File
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static Task<string> ReadAllTExt(string filePath)
		{
			if (filePath == null) throw new ArgumentNullException();
			string fileId = _dataManager.GetFileIdFromPath(filePath);
			return ReadAllTextById(fileId);
		}

		/// <summary>
		///     Read All Bytes from the File
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static Task<string> ReadAllTextById(string fileId)
		{
			if (fileId == null) throw new ArgumentNullException();
			return _blobStorage.ReadAllTextAsync(fileId);
		}

		/// <summary>
		///     Returns File size in Bytes
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileExistsException"></exception>
		public static int GetSize(string filePath)
		{
			if (filePath == null) throw new ArgumentNullException();
			string fileId = _dataManager.GetFileIdFromPath(filePath);
			return _blobStorage.GetSize(fileId);
		}

		/// <summary>
		///     Returns File size in Bytes
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileExistsException"></exception>
		public static int GetSizeById(string fileId)
		{
			if (fileId == null) throw new ArgumentNullException();
			return _blobStorage.GetSize(fileId);
		}

		/// <summary>
		///     returns the SHA256 Hash from a File
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static Task<byte[]> GetSHA256HashAsync(string filePath)
		{
			if (filePath == null) throw new ArgumentNullException();
			string fileId = _dataManager.GetFileIdFromPath(filePath);
			return _blobStorage.GetSHA256HashAsync(fileId);
		}

		/// <summary>
		///     returns the SHA256 Hash from a File
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static Task<byte[]> GetSHA256HashByIdAsync(string fileId)
		{
			if (fileId == null) throw new ArgumentNullException();
			return _blobStorage.GetSHA256HashAsync(fileId);
		}

		/// <summary>
		///     Returns the Parent Directory from the given File
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static LokiDirectoryInfo GetParentDirectoryById(string fileId)
		{
			if (fileId == null) throw new ArgumentNullException();
			LokiFileInfo file = _dataManager.GetFile(fileId);
			LokiDirectoryInfo result = _dataManager.GetDirectory(LokiPath.GetParentDirectory(file.Path));
			return result;
		}

		/// <summary>
		///     Returns the Parent Directory from the given File
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		public static LokiDirectoryInfo GetParentDirectory(string filePath)
		{
			if (filePath == null) throw new ArgumentNullException();
			string fileId = _dataManager.GetFileIdFromPath(filePath);
			return GetParentDirectoryById(fileId);
		}
	}
}