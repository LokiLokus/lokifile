namespace LokiFile {
	public class LokiDirectoryInfo {
		public string Id { get; set; }
		public string DirectoryName { get; set; }
		public string CustomData { get; set; }
		public bool IsRoot => Path == "/";
		public string Path { get; set; }
	}
}