using System;
using System.Collections.Generic;
using System.IO;

namespace LokiFile.DataManager {
	public interface IDataManager {

		
		/// <summary>
		///     Check for File
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException">Self-explained</exception>
		bool ExistsFile(string filePath);

		/// <summary>
		///     Check for Directory
		/// </summary>
		/// <param name="dirPath"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException">Self-explained</exception>
		bool ExistsDirectory(string dirPath);

		/// <summary>
		///     Returns ID for File by given LokiPath
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		/// <exception cref="FileNotFoundException">When File not Found</exception>
		/// <exception cref="ArgumentNullException">Self-explained</exception>
		string GetFileIdFromPath(string filePath);

		/// <summary>
		///     Returns ID for the last Directory
		///     e.g. /dir/test.txt => returns ID from dir/
		/// </summary>
		/// <param name="dirPath"></param>
		/// <returns></returns>
		/// <exception cref="DirectoryNotFoundException">When LokiPath/File not Found</exception>
		/// <exception cref="ArgumentNullException">Self-explained</exception>
		string GetDirectoryIdFromPath(string dirPath);

		/// <summary>
		///     Returns LokiFileInfo From File
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		LokiFileInfo GetFile(string fileId);

		/// <summary>
		///     Returns LokiFileInfo From File
		/// </summary>
		/// <param name="dirId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		LokiDirectoryInfo GetDirectory(string dirId);

		/// <summary>
		///     Creates a new LokiFile
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="customData"></param>
		/// <param name="dirId"></param>
		/// <param name="overwrite"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		/// <exception cref="NameNotValidException"></exception>
		/// <exception cref="FileExistsException"></exception>
		LokiFileInfo CreateFile(string filename, string customData, string dirId);

		/// <summary>
		///     Creates a new LokiDirectory
		/// </summary>
		/// <param name="dirname"></param>
		/// <param name="customData"></param>
		/// <param name="dirId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		/// <exception cref="NameNotValidException"></exception>
		/// <exception cref="DirectoryExistsException"></exception>
		LokiDirectoryInfo CreateDirectory(string dirname, string customData, string dirId);

		/// <summary>
		///     Updates the CustomData of the given File
		/// </summary>
		/// <param name="fileId"></param>
		/// <param name="data"></param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		void UpdateFileCustomData(string fileId, string data);

		/// <summary>
		///     Updates the CustomData of the given Directory
		/// </summary>
		/// <param name="dirId"></param>
		/// <param name="data"></param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		void UpdateDirectoryCustomData(string dirId, string data);

		/// <summary>
		///     DeleteFile The File
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns> returns a List of all File ID's in the Directory</returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		void DeleteFile(string fileId);


		/// <summary>
		///     DeleteFile The Directory
		/// </summary>
		/// <param name="dirId"></param>
		/// <param name="deleteRecursive"></param>
		/// <returns> returns a List of all File ID's in the Directory</returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		/// <exception cref="DirectoryNotEmptyException"></exception>
		void DeleteDirectory(string dirId, bool deleteRecursive);

		/// <summary>
		///     Returns the Files in the Directory
		/// </summary>
		/// <param name="dirId"></param>
		/// <param name="recursive"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		IEnumerable<LokiFileInfo> GetFiles(string dirId, bool recursive);

		/// <summary>
		///     Returns the Directories in the Directory
		/// </summary>
		/// <param name="dirId"></param>
		/// <param name="recursive"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		IEnumerable<LokiDirectoryInfo> GetDirectories(string dirId, bool recursive);


		/// <summary>
		///     Returns a bool if the Directory is Empty
		/// </summary>
		/// <param name="dirId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		bool DirectoryIsEmpty(string dirId);
	}
}