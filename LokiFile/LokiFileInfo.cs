namespace LokiFile {
	public class LokiFileInfo {
		public string Id { get; set; }
		public string Name { get; set; }
		public string CustomData { get; set; }
		public string Path { get; set; }
	}
}