using System;
using System.Collections.Generic;
using LokiFile.BlobStorage;
using LokiFile.DataManager;

namespace LokiFile {
	public static class LokiDirectory {
		private static IDataManager _dataManager => LokiFile._dataManager;
		private static IBlobStorage _blobStorage => LokiFile._blobStorage;


		/// <summary>
		///     DeleteFile the given Directory not empty
		/// </summary>
		/// <param name="path"></param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		public static void Delete(string path)
		{
			Delete(path, false);
		}

		public static void Delete(string path, bool deleteRecursive)
		{
			if(path == null) throw new ArgumentNullException();
			path = LokiPath.NormalizePath(path);
			if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException();
			if (!Exists(path)) throw new DirectoryNotFoundException();
			string dirId = _dataManager.GetDirectoryIdFromPath(path);
			if (!deleteRecursive &&
			    _dataManager.DirectoryIsEmpty(dirId))
				throw new DirectoryNotEmptyException();

			IEnumerable<string> files = _dataManager.DeleteDirectory(dirId, deleteRecursive);
			foreach (string file in files)
			{
				_dataManager.DeleteFile(file);
				_blobStorage.DeleteFile(file);
			}
		}

		/// <summary>
		///     Checks wether Directory exists
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		public static bool Exists(string path)
		{
			if(string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException();
			path = LokiPath.NormalizePath(path);
			if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException();
			return _dataManager.ExistsDirectory(path);
		}

		public static LokiDirectoryInfo GetDirectory(string path)
		{
			if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException();
			path = LokiPath.NormalizePath(path);
			bool dirExists = Exists(path);
			if (!dirExists) throw new DirectoryNotFoundException();
			string id = _dataManager.GetDirectoryIdFromPath(path);
			return _dataManager.GetDirectory(id);
		}

		public static LokiDirectoryInfo GetDirectoryById(string id)
		{
			if (string.IsNullOrWhiteSpace(id)) throw new ArgumentNullException();
			return _dataManager.GetDirectory(id);
		}

		public static LokiDirectoryInfo GetParent(string path)
		{
			if(string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException();
			path = LokiPath.NormalizePath(path);
			return GetParentById(GetDirectory(path).Id);
		}

		public static LokiDirectoryInfo GetParentById(string id)
		{
			if (id == null) throw new ArgumentNullException();
			LokiDirectoryInfo tmp = _dataManager.GetDirectory(id);
			string parentPath = LokiPath.GetParentDirectory(tmp.Path);
			string dirId = _dataManager.GetDirectoryIdFromPath(parentPath);
			return _dataManager.GetDirectory(dirId);
		}

		public static IEnumerable<LokiDirectoryInfo> GetDirectories(string path, bool recursive = false)
		{
			if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException();
			path = LokiPath.NormalizePath(path);
			return GetDirectoriesById(GetDirectory(path).Id,recursive);
		}

		public static IEnumerable<LokiDirectoryInfo> GetDirectoriesById(string id, bool recursive = false)
		{
			if (string.IsNullOrWhiteSpace(id)) throw new ArgumentNullException();
			return _dataManager.GetDirectories(id, recursive);
		}

		public static IEnumerable<LokiFileInfo> GetFiles(string path, bool recursive = false)
		{
			if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException();
			path = LokiPath.NormalizePath(path);
			return GetFilesById(GetDirectory(path).Id);
		}

		public static IEnumerable<LokiFileInfo> GetFilesById(string id, bool recursive = false)
		{
			if (string.IsNullOrWhiteSpace(id)) throw new ArgumentNullException();
			return _dataManager.GetFiles(id, recursive);
		}

		public static LokiDirectoryInfo CreateDirectory(string dirName,string customData = null)
		{
			if(dirName == null) throw new ArgumentNullException();
			if(!LokiPath.DirectoryPathValid(dirName)) throw new PathNotValidException();
			if(_dataManager.ExistsDirectory(dirName)) throw new DirectoryExistsException(dirName);
			dirName = LokiPath.NormalizePath(dirName);
			string parentDir = LokiPath.GetParentDirectory(dirName);
			string parentDirId = _dataManager.GetDirectoryIdFromPath(parentDir);

			string dirShortName = LokiPath.GetCurrentDirectoryFromPath(dirName);
			
			return _dataManager.CreateDirectory(dirShortName, customData, parentDirId);
		}
	}

	public class NotInitilizedException : Exception {
	}

	public class NameNotValidException : Exception {
	}

	public class DirectoryNotFoundException : Exception {
	}

	public class DirectoryNotNullException : Exception {
	}

	public class FileExistsException : Exception {
	}

	public class DirectoryExistsException : Exception {
		public DirectoryExistsException(string dir)
		{
			Directory = dir;
		}

		public string Directory { get; set; }
	}

	public class DirectoryNotEmptyException : Exception {
	}
}