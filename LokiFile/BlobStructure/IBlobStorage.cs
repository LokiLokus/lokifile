using System;
using System.IO;
using System.Threading.Tasks;

namespace LokiFile.BlobStorage {
	public interface IBlobStorage {
		/// <summary>
		///     Writes Data to File
		/// </summary>
		/// <param name="fileId"></param>
		/// <param name="data"></param>
		/// <param name="fMode"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		Task WriteAsync(string fileId, string data, FileAccessMode fMode);

		/// <summary>
		///     Writes Data to File
		/// </summary>
		/// <param name="fileId"></param>
		/// <param name="data"></param>
		/// <param name="fMode"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		Task WriteAsync(string fileId, byte[] data, FileAccessMode fMode);

		/// <summary>
		///     Creates the File with Id
		/// </summary>
		/// <param name="fileId"></param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileExistsException"></exception>
		void CreateFile(string fileId);

		/// <summary>
		///     Returns File size in Bytes
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException">When File not Found</exception>
		int GetSize(string fileId);

		/// <summary>
		///     Deletes the file with given FileId
		/// </summary>
		/// <param name="fileId"></param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		void DeleteFile(string fileId);

		/// <summary>
		///     Read All Bytes from the File
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		Task<byte[]> ReadAllBytesAsync(string fileId);

		/// <summary>
		///     Read All Bytes from the File
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		Task<string> ReadAllTextAsync(string fileId);

		/// <summary>
		///     returns the SHA256 Hash from a File
		/// </summary>
		/// <param name="fileId"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		Task<byte[]> GetSHA256HashAsync(string fileId);
	}
}