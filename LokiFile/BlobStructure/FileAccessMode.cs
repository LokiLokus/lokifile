namespace LokiFile.BlobStorage {
	public enum FileAccessMode {
		Overwrite,
		Append
	}
}