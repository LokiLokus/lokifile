using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace LokiFile {
	public static class LokiPath {
		public const string SEPERATOR = "/";
		public const string ROOT_DIRECTORY_PATH = "/";

		internal static string GetFileNameFromPath(string path)
		{
			return Path.GetFileName(path);
		}

		public static string GetCurrentDirectoryFromPath(string path)
		{
			if (!DirectoryPathValid(path)) throw new PathNotValidException();
			if (path == ROOT_DIRECTORY_PATH) return ROOT_DIRECTORY_PATH;
			string[] paths = SplitPath(path);
			return paths.Skip(paths.Length-2).Take(1).SingleOrDefault();
		}
		
		

		public static string GetParentDirectory(string filePath)
		{
			filePath = LokiPath.NormalizePath(filePath);
			if (FilePathIsValid(filePath))
			{
				var paths = SplitPath(filePath).ToList();
				if(paths.Count < 1) throw new PathNotValidException();

				paths.Remove(paths.Last());
				return SEPERATOR + String.Join(SEPERATOR,paths);
				
			}else if (DirectoryPathValid(filePath))
			{
				if (filePath == ROOT_DIRECTORY_PATH) return ROOT_DIRECTORY_PATH;
				var paths = SplitPath(filePath).ToList();
				if(paths.Count < 1) throw new PathNotValidException();
				string result = ROOT_DIRECTORY_PATH;
				for (int i = 1; i < paths.Count-2; i++)
				{
					result += paths[i] + SEPERATOR;
				}
				return result;

			}else throw new PathNotValidException();
		}

		public static string[] SplitPath(string filePath)
		{
			return filePath.Split(SEPERATOR);
		}

		public static bool IsFileNameValid(string fileName)
		{
			if (string.IsNullOrWhiteSpace(fileName)) return false;
			return Regex.Match(fileName,  @"^([a-zA-Z0-9]{1,20}.[a-zA-Z0-9]{1,5})$").Success;
		}

		public static bool IsDirectoryNameValid(string dirName)
		{
			if (string.IsNullOrWhiteSpace(dirName)) return false;
			return Regex.Match(dirName,  @"^([a-zA-Z0-9]){1,20}$").Success;
		}

		public static bool DirectoryPathValid(string dirPath)
		{
			if (string.IsNullOrWhiteSpace(dirPath)) return false;
			if (dirPath == ROOT_DIRECTORY_PATH) return true;
			return Regex.Match(dirPath,  @"^/([a-zA-Z0-9]{1,20}/){0,10}([a-zA-Z0-9]{1,20}/?)$").Success;
		}

		public static bool FilePathIsValid(string filePath)
		{
			if (string.IsNullOrWhiteSpace(filePath)) return false;
			return Regex.Match(filePath,  @"^/([a-zA-Z0-9]{1,20}/){0,11}[a-zA-Z0-9]{1,20}\.[a-zA-Z0-9]{1,5}$").Success;
		}

		public static string NormalizePath(string path)
		{
			if (FilePathIsValid(path))
			{
				return path;
			}else if(DirectoryPathValid(path))
			{
				if (path.EndsWith(SEPERATOR)) return path;
				return path + SEPERATOR;
			}
			throw new PathNotValidException();
		}
	}

	public class PathNotValidException : Exception
	{
	}
}