using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using LokiFile;
using LokiFile.BlobStorage;

namespace LokiFileBlobManager {
	public class FileBlobStorage : IBlobStorage {
		private readonly string _path;

		public FileBlobStorage(string path)
		{
			_path = path;
		}

		public Task WriteAsync(string fileId, string data, FileAccessMode fMode)
		{
			string filePath = Path.Combine(_path, fileId);
			if (!File.Exists(filePath)) throw new FileNotFoundException();
			if (fMode == FileAccessMode.Overwrite)
				return File.WriteAllTextAsync(filePath, data);
			return File.AppendAllTextAsync(filePath, data);
		}

		public Task WriteAsync(string fileId, byte[] data, FileAccessMode fMode)
		{
			string filePath = Path.Combine(_path, fileId);
			if (!File.Exists(filePath)) throw new FileNotFoundException();
			if (fMode == FileAccessMode.Overwrite)
				return File.WriteAllBytesAsync(filePath, data);
			return File.WriteAllBytesAsync(filePath, data);
		}

		public void CreateFile(string fileId)
		{
			string filePath = Path.Combine(_path, fileId);
			if (File.Exists(filePath)) throw new FileExistsException();
			File.WriteAllText(filePath, "");
		}

		public int GetSize(string fileId)
		{
			string filePath = Path.Combine(_path, fileId);
			if (!File.Exists(filePath)) throw new FileNotFoundException();
			return File.ReadAllBytes(filePath).Length;
		}

		public void DeleteFile(string fileId)
		{
			string filePath = Path.Combine(_path, fileId);
			if (!File.Exists(filePath)) throw new FileNotFoundException();
			File.Delete(Path.Combine(_path, fileId));
		}

		public Task<byte[]> ReadAllBytesAsync(string fileId)
		{
			string filePath = Path.Combine(_path, fileId);
			if (!File.Exists(filePath)) throw new FileNotFoundException();
			return File.ReadAllBytesAsync(fileId);
		}

		public Task<string> ReadAllTextAsync(string fileId)
		{
			string filePath = Path.Combine(_path, fileId);
			if (!File.Exists(filePath)) throw new FileNotFoundException();
			return File.ReadAllTextAsync(fileId);
		}

		public async Task<byte[]> GetSHA256HashAsync(string fileId)
		{
			string filePath = Path.Combine(_path, fileId);
			if (!File.Exists(filePath)) throw new FileNotFoundException();
			SHA256Managed hasher = new SHA256Managed();
			return hasher.ComputeHash(await ReadAllBytesAsync(filePath));
		}
	}
}